package edu.ucsd.cse110.library;

import java.util.ArrayList;
import java.util.List;

public class Member {
	private MemberType memberType;
	private String name;
	private double fees; 
	private ArrayList<Publication> pubs = new ArrayList<Publication>();
	
	public Member(String string, MemberType type) {
		setName(string);
		memberType = type;
	}

	public double getDueFees() {
		return fees;
	}

	public void applyLateFee(double i) {
		fees+=i;		
	}
	
	public MemberType getType() {
		return memberType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public boolean hasPublication(Publication p){
		for(int i=0; i < pubs.size(); i++){
			if(p == pubs.get(i)){
				return true;
			}
		}
		return false;
	}
	
	public void checkingOut(Publication p){
		pubs.add(p);
	}
	
	public void returning(Publication p) {
		pubs.remove(p);
	}

}
