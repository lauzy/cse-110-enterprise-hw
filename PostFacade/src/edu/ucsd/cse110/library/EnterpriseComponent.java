package edu.ucsd.cse110.library;

import java.time.LocalDate;

public class EnterpriseComponent implements FacadeInterface
{
	public void checkoutPublication(Member m, Publication p) {
		LocalDate checkoutDate = LocalDate.now();
		                                                   
		p.checkout(m, checkoutDate);
		m.checkingOut(p);
	}

	public void returnPublication(Publication p) {
		LocalDate returnDate = LocalDate.now();
		                                                 
		p.getMember().returning(p);
		p.pubReturn(returnDate);

	}

	public double getFee(Member m) {
		return m.getDueFees();
	}

	public boolean hasFee(Member m) {
		return m.getDueFees() > 0;
	}
	
	public boolean isCheckedOut(Publication p) {
		return p.isCheckout();
	}

}
