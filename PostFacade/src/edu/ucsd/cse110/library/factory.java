package edu.ucsd.cse110.library;

public class factory {
	public FacadeInterface make(String s){
		if(s.equals("enterpriseComponent")){
			return new EnterpriseComponent();
		}
		return null;
	}
}
