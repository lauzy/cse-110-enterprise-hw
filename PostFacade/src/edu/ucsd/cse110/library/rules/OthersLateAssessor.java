package edu.ucsd.cse110.library.rules;

import edu.ucsd.cse110.library.MemberType;


public class OthersLateAssessor implements Assessor {

	public boolean evaluate(Properties prop) {
		return prop.getType() == MemberType.Other 
				&& prop.getDays()>7;
	}

	public String getErrors() {
		return null;
	}

}
