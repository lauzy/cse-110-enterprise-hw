package edu.ucsd.cse110.library.rules;

public class ChargePerDayFeeAction implements Action {
	private double perDayFee;
	private int freeDays;
	
	public ChargePerDayFeeAction(double perDayFee, int freeDays) {
		this.perDayFee = perDayFee;
		this.freeDays = freeDays;
	}

	public void execute(Properties prop) {
		double assessed = (prop.getDays()-freeDays)*perDayFee;
		prop.getMember().applyLateFee(assessed);
	}

	public String getErrors() {
		return null;
	}

}
