package edu.ucsd.cse110.library.rules;


public class RuleObject implements IRuleObject {
	private Assessor assessor;
	private Action action;
	private Result result;
	
	public RuleObject(Assessor assessor, Action action) {
		this.assessor = assessor;
		this.action = action;
		this.result = new Result();
	}

	public boolean checkRule(Properties prop) {
		
		if(prop == null) System.exit(-1);

		
		if(assessor.evaluate(prop)) {
			action.execute(prop);
			result.setActionResults(action.getErrors());
			result.setAssessorResults(assessor.getErrors());
			return true;
		}
		result.setAssessorResults(assessor.getErrors());
		return false;
	}

	public Result getResults() {
		return result;
	}

}
