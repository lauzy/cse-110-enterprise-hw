package edu.ucsd.cse110.library;


public class Book extends Publication {
	private String title;

	public Book(String string, LateFeesStrategy lateFees) {
		super(lateFees);
		title = string;
	}

}
