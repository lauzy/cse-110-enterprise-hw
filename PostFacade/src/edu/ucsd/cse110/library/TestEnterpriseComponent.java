package edu.ucsd.cse110.library;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import java.text.ParseException;
import java.time.LocalDate;

import edu.ucsd.cse110.library.rules.*;

public class TestEnterpriseComponent {
	
    Member member1, member2;
    Publication book1, book2;
    FacadeInterface geisel;
   
	
	@Before
	public void setUp(){
		member1 = new Member("Eric", MemberType.Teacher);
		member2 = new Member("Sal", MemberType.Teacher);
		
		book1 = new Book("Harry Potter and the Sorceror's Stone", new RuleObjectBasedLateFeesStrategy());
		book2 = new Book("The Hobbit", new RuleObjectBasedLateFeesStrategy());
		
	    factory factory = new factory();
		geisel = factory.make("enterpriseComponent");			
	}
	
	@Test
	public void testCheckoutPublication() throws ParseException {
		geisel.checkoutPublication(member1, book1);
		assertTrue(member1.hasPublication(book1));
	}
	
	
	@Test
	public void testReturnPublication() throws ParseException {
		geisel.checkoutPublication(member1, book1);
		assertTrue(member1.hasPublication(book1));		
		
		 geisel.returnPublication(book1);
			
		    assertFalse(member1.hasPublication(book1));
			
	        assertFalse(book1.isCheckout());
			assertEquals(2.00, member1.getDueFees(), 0.01);
			
			
			assertTrue(geisel.hasFee(member1));
			assertTrue(geisel.getFee(member1) == 2.0);
			
	}
	
	@Test
	public void testGetFee() throws ParseException {
	}


	@Test
	public void testHasFee() throws ParseException {
	}
	
}
